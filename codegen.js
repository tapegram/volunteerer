module.exports = {
    schema: [
        {
            'https://graphql.fauna.com/graphql': {
                headers: {
                    Authorization: 'Bearer ' + "fnAD9tm9H9ACBgm9f_vEZ2efQAZ8cWp1enN_cIsI",
                },
            },
        },
    ],
    documents: ['./data/fauna/*.graphql'],
    overwrite: true,
    generates: {
        './data/fauna/generated/graphql.tsx': {
            plugins: [
                'typescript',
                'typescript-operations',
                'typed-document-node',
            ],
            config: {
                skipTypename: false,
                withHooks: true,
                withHOC: false,
                withComponent: false,
                avoidOptionals: true,
                // maybeValue: 'T | null'
            },
        },
        './data/fauna/graphql.schema.json': {
            plugins: ['introspection'],
        },
    }
};