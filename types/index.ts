export type Group = {
  id: string,
  name: string,
  members: Member[]
}

export type Member = {
  id: string,
  name: string,
  selections: Selection[]
}

export type Selection = {
  id: string,
  on: Date,
  for: string,
}