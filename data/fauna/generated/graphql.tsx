import { TypedDocumentNode as DocumentNode } from '@graphql-typed-document-node/core';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  Date: any;
  Time: any;
  /** The `Long` scalar type represents non-fractional signed whole numeric values. Long can represent values between -(2^63) and 2^63 - 1. */
  Long: any;
};








/** 'Group' input values */
export type GroupInput = {
  name: Scalars['String'];
  members: Maybe<GroupMembersRelation>;
};

/** Allow manipulating the relationship between the types 'Group' and 'Member'. */
export type GroupMembersRelation = {
  /** Create one or more documents of type 'Member' and associate them with the current document. */
  create: Maybe<Array<Maybe<MemberInput>>>;
  /** Connect one or more documents of type 'Member' with the current document using their IDs. */
  connect: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Disconnect the given documents of type 'Member' from the current document using their IDs. */
  disconnect: Maybe<Array<Maybe<Scalars['ID']>>>;
};

/** Allow manipulating the relationship between the types 'Member' and 'Group' using the field 'Member.group'. */
export type MemberGroupRelation = {
  /** Create a document of type 'Group' and associate it with the current document. */
  create: Maybe<GroupInput>;
  /** Connect a document of type 'Group' with the current document using its ID. */
  connect: Maybe<Scalars['ID']>;
};

/** 'Member' input values */
export type MemberInput = {
  name: Scalars['String'];
  group: Maybe<MemberGroupRelation>;
  selections: Maybe<MemberSelectionsRelation>;
};

/** Allow manipulating the relationship between the types 'Member' and 'Selection'. */
export type MemberSelectionsRelation = {
  /** Create one or more documents of type 'Selection' and associate them with the current document. */
  create: Maybe<Array<Maybe<SelectionInput>>>;
  /** Connect one or more documents of type 'Selection' with the current document using their IDs. */
  connect: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Disconnect the given documents of type 'Selection' from the current document using their IDs. */
  disconnect: Maybe<Array<Maybe<Scalars['ID']>>>;
};

export type Mutation = {
  __typename?: 'Mutation';
  addMember: Group;
  /** Update an existing document in the collection of 'Selection' */
  updateSelection: Maybe<Selection>;
  newGroup: Group;
  /** Create a new document in the collection of 'Selection' */
  createSelection: Selection;
  /** Create a new document in the collection of 'Group' */
  createGroup: Group;
  /** Create a new document in the collection of 'Member' */
  createMember: Member;
  /** Update an existing document in the collection of 'Group' */
  updateGroup: Maybe<Group>;
  /** Update an existing document in the collection of 'Member' */
  updateMember: Maybe<Member>;
  /** Delete an existing document in the collection of 'Selection' */
  deleteSelection: Maybe<Selection>;
  removeMember: Group;
  /** Delete an existing document in the collection of 'Group' */
  deleteGroup: Maybe<Group>;
  /** Delete an existing document in the collection of 'Member' */
  deleteMember: Maybe<Member>;
};


export type MutationAddMemberArgs = {
  groupId: Scalars['ID'];
  name: Scalars['String'];
};


export type MutationUpdateSelectionArgs = {
  id: Scalars['ID'];
  data: SelectionInput;
};


export type MutationNewGroupArgs = {
  name: Scalars['String'];
};


export type MutationCreateSelectionArgs = {
  data: SelectionInput;
};


export type MutationCreateGroupArgs = {
  data: GroupInput;
};


export type MutationCreateMemberArgs = {
  data: MemberInput;
};


export type MutationUpdateGroupArgs = {
  id: Scalars['ID'];
  data: GroupInput;
};


export type MutationUpdateMemberArgs = {
  id: Scalars['ID'];
  data: MemberInput;
};


export type MutationDeleteSelectionArgs = {
  id: Scalars['ID'];
};


export type MutationRemoveMemberArgs = {
  groupId: Scalars['ID'];
  memberId: Scalars['ID'];
};


export type MutationDeleteGroupArgs = {
  id: Scalars['ID'];
};


export type MutationDeleteMemberArgs = {
  id: Scalars['ID'];
};

/** 'Selection' input values */
export type SelectionInput = {
  on: Scalars['Time'];
  for: Maybe<Scalars['String']>;
  member: Maybe<SelectionMemberRelation>;
};

/** Allow manipulating the relationship between the types 'Selection' and 'Member' using the field 'Selection.member'. */
export type SelectionMemberRelation = {
  /** Create a document of type 'Member' and associate it with the current document. */
  create: Maybe<MemberInput>;
  /** Connect a document of type 'Member' with the current document using its ID. */
  connect: Maybe<Scalars['ID']>;
};

export type Group = {
  __typename?: 'Group';
  /** The document's ID. */
  _id: Scalars['ID'];
  /** The document's timestamp. */
  _ts: Scalars['Long'];
  name: Scalars['String'];
  members: MemberPage;
};


export type GroupMembersArgs = {
  _size: Maybe<Scalars['Int']>;
  _cursor: Maybe<Scalars['String']>;
};

/** The pagination object for elements of type 'Group'. */
export type GroupPage = {
  __typename?: 'GroupPage';
  /** The elements of type 'Group' in this page. */
  data: Array<Maybe<Group>>;
  /** A cursor for elements coming after the current page. */
  after: Maybe<Scalars['String']>;
  /** A cursor for elements coming before the current page. */
  before: Maybe<Scalars['String']>;
};

export type Member = {
  __typename?: 'Member';
  name: Scalars['String'];
  /** The document's ID. */
  _id: Scalars['ID'];
  selections: SelectionPage;
  group: Group;
  /** The document's timestamp. */
  _ts: Scalars['Long'];
};


export type MemberSelectionsArgs = {
  _size: Maybe<Scalars['Int']>;
  _cursor: Maybe<Scalars['String']>;
};

/** The pagination object for elements of type 'Member'. */
export type MemberPage = {
  __typename?: 'MemberPage';
  /** The elements of type 'Member' in this page. */
  data: Array<Maybe<Member>>;
  /** A cursor for elements coming after the current page. */
  after: Maybe<Scalars['String']>;
  /** A cursor for elements coming before the current page. */
  before: Maybe<Scalars['String']>;
};

export type Query = {
  __typename?: 'Query';
  /** Find a document from the collection of 'Selection' by its id. */
  findSelectionByID: Maybe<Selection>;
  /** Find a document from the collection of 'Member' by its id. */
  findMemberByID: Maybe<Member>;
  /** Find a document from the collection of 'Group' by its id. */
  findGroupByID: Maybe<Group>;
  allGroups: GroupPage;
};


export type QueryFindSelectionByIdArgs = {
  id: Scalars['ID'];
};


export type QueryFindMemberByIdArgs = {
  id: Scalars['ID'];
};


export type QueryFindGroupByIdArgs = {
  id: Scalars['ID'];
};


export type QueryAllGroupsArgs = {
  _size: Maybe<Scalars['Int']>;
  _cursor: Maybe<Scalars['String']>;
};

export type Selection = {
  __typename?: 'Selection';
  for: Maybe<Scalars['String']>;
  /** The document's ID. */
  _id: Scalars['ID'];
  on: Scalars['Time'];
  member: Member;
  /** The document's timestamp. */
  _ts: Scalars['Long'];
};

/** The pagination object for elements of type 'Selection'. */
export type SelectionPage = {
  __typename?: 'SelectionPage';
  /** The elements of type 'Selection' in this page. */
  data: Array<Maybe<Selection>>;
  /** A cursor for elements coming after the current page. */
  after: Maybe<Scalars['String']>;
  /** A cursor for elements coming before the current page. */
  before: Maybe<Scalars['String']>;
};



export type AllGroupsQueryVariables = Exact<{ [key: string]: never; }>;


export type AllGroupsQuery = (
  { __typename?: 'Query' }
  & { allGroups: (
    { __typename?: 'GroupPage' }
    & Pick<GroupPage, 'after' | 'before'>
    & { data: Array<Maybe<(
      { __typename?: 'Group' }
      & Pick<Group, '_id' | '_ts' | 'name'>
      & { members: (
        { __typename?: 'MemberPage' }
        & Pick<MemberPage, 'after' | 'before'>
        & { data: Array<Maybe<(
          { __typename?: 'Member' }
          & Pick<Member, '_id' | '_ts' | 'name'>
          & { selections: (
            { __typename?: 'SelectionPage' }
            & Pick<SelectionPage, 'after' | 'before'>
            & { data: Array<Maybe<(
              { __typename?: 'Selection' }
              & Pick<Selection, '_id' | '_ts' | 'on' | 'for'>
            )>> }
          ) }
        )>> }
      ) }
    )>> }
  ) }
);

export type CreateGroupMutationVariables = Exact<{
  name: Scalars['String'];
}>;


export type CreateGroupMutation = (
  { __typename?: 'Mutation' }
  & { createGroup: (
    { __typename?: 'Group' }
    & Pick<Group, '_id' | '_ts' | 'name'>
    & { members: (
      { __typename?: 'MemberPage' }
      & Pick<MemberPage, 'before' | 'after'>
      & { data: Array<Maybe<(
        { __typename?: 'Member' }
        & Pick<Member, '_id' | '_ts' | 'name'>
        & { selections: (
          { __typename?: 'SelectionPage' }
          & Pick<SelectionPage, 'before' | 'after'>
          & { data: Array<Maybe<(
            { __typename?: 'Selection' }
            & Pick<Selection, '_id' | '_ts' | 'on' | 'for'>
          )>> }
        ) }
      )>> }
    ) }
  ) }
);

export type CreateSelectionMutationVariables = Exact<{
  for: Scalars['String'];
  on: Scalars['Time'];
  memberId: Scalars['ID'];
}>;


export type CreateSelectionMutation = (
  { __typename?: 'Mutation' }
  & { createSelection: (
    { __typename?: 'Selection' }
    & Pick<Selection, '_id' | '_ts' | 'for' | 'on'>
  ) }
);

export type CreateMemberMutationVariables = Exact<{
  name: Scalars['String'];
  groupId: Scalars['ID'];
}>;


export type CreateMemberMutation = (
  { __typename?: 'Mutation' }
  & { createMember: (
    { __typename?: 'Member' }
    & Pick<Member, '_id' | '_ts' | 'name'>
    & { selections: (
      { __typename?: 'SelectionPage' }
      & Pick<SelectionPage, 'before' | 'after'>
      & { data: Array<Maybe<(
        { __typename?: 'Selection' }
        & Pick<Selection, '_id' | '_ts' | 'on' | 'for'>
      )>> }
    ) }
  ) }
);

export type FindGroupByIdQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type FindGroupByIdQuery = (
  { __typename?: 'Query' }
  & { findGroupByID: Maybe<(
    { __typename?: 'Group' }
    & Pick<Group, '_id' | '_ts' | 'name'>
    & { members: (
      { __typename?: 'MemberPage' }
      & Pick<MemberPage, 'before' | 'after'>
      & { data: Array<Maybe<(
        { __typename?: 'Member' }
        & Pick<Member, '_id' | '_ts' | 'name'>
        & { selections: (
          { __typename?: 'SelectionPage' }
          & Pick<SelectionPage, 'before' | 'after'>
          & { data: Array<Maybe<(
            { __typename?: 'Selection' }
            & Pick<Selection, '_id' | '_ts' | 'on' | 'for'>
          )>> }
        ) }
      )>> }
    ) }
  )> }
);


export const AllGroupsDocument: DocumentNode<AllGroupsQuery, AllGroupsQueryVariables> = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"allGroups"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"allGroups"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"after"}},{"kind":"Field","name":{"kind":"Name","value":"before"}},{"kind":"Field","name":{"kind":"Name","value":"data"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"_ts"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"members"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"after"}},{"kind":"Field","name":{"kind":"Name","value":"before"}},{"kind":"Field","name":{"kind":"Name","value":"data"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"_ts"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"selections"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"after"}},{"kind":"Field","name":{"kind":"Name","value":"before"}},{"kind":"Field","name":{"kind":"Name","value":"data"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"_ts"}},{"kind":"Field","name":{"kind":"Name","value":"on"}},{"kind":"Field","name":{"kind":"Name","value":"for"}}]}}]}}]}}]}}]}}]}}]}}]};
export const CreateGroupDocument: DocumentNode<CreateGroupMutation, CreateGroupMutationVariables> = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"createGroup"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"name"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"createGroup"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"data"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"name"},"value":{"kind":"Variable","name":{"kind":"Name","value":"name"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"_ts"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"members"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"before"}},{"kind":"Field","name":{"kind":"Name","value":"after"}},{"kind":"Field","name":{"kind":"Name","value":"data"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"_ts"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"selections"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"before"}},{"kind":"Field","name":{"kind":"Name","value":"after"}},{"kind":"Field","name":{"kind":"Name","value":"data"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"_ts"}},{"kind":"Field","name":{"kind":"Name","value":"on"}},{"kind":"Field","name":{"kind":"Name","value":"for"}}]}}]}}]}}]}}]}}]}}]};
export const CreateSelectionDocument: DocumentNode<CreateSelectionMutation, CreateSelectionMutationVariables> = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"createSelection"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"for"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"on"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Time"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"memberId"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"createSelection"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"data"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"for"},"value":{"kind":"Variable","name":{"kind":"Name","value":"for"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"on"},"value":{"kind":"Variable","name":{"kind":"Name","value":"on"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"member"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"connect"},"value":{"kind":"Variable","name":{"kind":"Name","value":"memberId"}}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"_ts"}},{"kind":"Field","name":{"kind":"Name","value":"for"}},{"kind":"Field","name":{"kind":"Name","value":"on"}}]}}]}}]};
export const CreateMemberDocument: DocumentNode<CreateMemberMutation, CreateMemberMutationVariables> = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"createMember"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"name"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"groupId"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"createMember"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"data"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"name"},"value":{"kind":"Variable","name":{"kind":"Name","value":"name"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"group"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"connect"},"value":{"kind":"Variable","name":{"kind":"Name","value":"groupId"}}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"_ts"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"selections"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"before"}},{"kind":"Field","name":{"kind":"Name","value":"after"}},{"kind":"Field","name":{"kind":"Name","value":"data"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"_ts"}},{"kind":"Field","name":{"kind":"Name","value":"on"}},{"kind":"Field","name":{"kind":"Name","value":"for"}}]}}]}}]}}]}}]};
export const FindGroupByIdDocument: DocumentNode<FindGroupByIdQuery, FindGroupByIdQueryVariables> = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"findGroupByID"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"findGroupByID"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"_ts"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"members"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"before"}},{"kind":"Field","name":{"kind":"Name","value":"after"}},{"kind":"Field","name":{"kind":"Name","value":"data"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"_ts"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"selections"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"before"}},{"kind":"Field","name":{"kind":"Name","value":"after"}},{"kind":"Field","name":{"kind":"Name","value":"data"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"_id"}},{"kind":"Field","name":{"kind":"Name","value":"_ts"}},{"kind":"Field","name":{"kind":"Name","value":"on"}},{"kind":"Field","name":{"kind":"Name","value":"for"}}]}}]}}]}}]}}]}}]}}]};