import * as T from "../../types";
import * as gql from "../../data/fauna/generated/graphql";
import { ApolloClient, NormalizedCacheObject } from "@apollo/client";
import { EitherAsync } from "purify-ts/EitherAsync";
import { GroupsRepo, Failure } from "../../behavior/GroupsRepo";

type Client = ApolloClient<NormalizedCacheObject>;

const toFaunaGroupsRepo = (apolloClient: Client): GroupsRepo => {
  const client = apolloClient;
  return {
    fetchAll: fetchAll(client),
    findById: (id: string) => findById(client, id),
  };
};

const fetchAll = (
  apolloClient: Client
): (() => EitherAsync<Failure, T.Group[]>) => {
  return () =>
    EitherAsync(() => {
      try {
        return apolloClient.query({
          query: gql.AllGroupsDocument,
        });
      } catch {
        throw Error;
      }
    })
      .mapLeft((_) => "Failed")
      .map((result) =>
        result.data!.allGroups.data!.map((group) => toGroup(group as gql.Group))
      );
};

const findById = (
  apolloClient: Client,
  id: string
): EitherAsync<Failure, T.Group> =>
  EitherAsync(() => {
    try {
      return apolloClient.query({
        query: gql.FindGroupByIdDocument,
        variables: {
          id: id,
        },
      });
    } catch {
      throw Error;
    }
  })
    .mapLeft((_) => "Failed")
    // This ignores the possibility that the return value is null
    .map((result) => toGroup(result.data.findGroupByID as gql.Group));

export const toGroup = (group: gql.Group): T.Group => ({
  id: group._id,
  name: group.name,
  members: group.members.data?.map((member) => toMember(member as gql.Member)),
});

export const toMember = (member: gql.Member): T.Member => ({
  id: member._id,
  name: member.name,
  selections: member.selections.data?.map((selection) =>
    toSelection(selection as gql.Selection)
  ),
});

export const toSelection = (selection: gql.Selection): T.Selection => ({
  id: selection._id,
  on: selection.on,
  for: selection.for as string,
});

export default toFaunaGroupsRepo;
