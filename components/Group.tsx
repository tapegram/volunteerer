import * as T from "../types";

type Props = {
  group: T.Group;
};

const Group = (props: Props) => (
  <>
    <h1 className="mx-auto">{props.group.name}</h1>
    <table className="bg-white pb-4 px-4 rounded-md w-full">
      <thead>
        <tr className="rounded-lg text-sm font-medium text-gray-700 text-left">
          <th className="px-4 py-2">Name</th>
        </tr>
      </thead>
      <tbody className="text-sm font-normal text-gray-700">
        {props.group.members.map(toTableRow)}
      </tbody>
    </table>
  </>
);

const toTableRow = (member: T.Member) => (
  <tr
    key={member.id}
    className="hover:bg-gray-100 border-b border-gray-200 py-10"
  >
    <td className="px-4 py-4">{member.name}</td>
  </tr>
);

export default Group;
