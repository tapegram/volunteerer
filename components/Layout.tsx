import { FunctionComponent } from "react";
import NavBar from "./NavBar";

type Props = {};

const Layout: FunctionComponent<Props> = ({ children }) => (
  <div className="container mx-auto">
    <NavBar />
    {children}
  </div>
);

export default Layout;
