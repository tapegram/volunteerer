import * as T from "../types";
import CreateMemberForm from "../components/CreateMemberForm";
import Group from "../components/Group";
import Selections, { Selection } from "../components/Selections";
import Volunteer from "../components/Volunteer";
import {
  addMember,
  addSelection,
  updateMemberInGroup,
} from "../behavior/Index";
import { useState } from "react";

type Props = {
  group: T.Group;
};

const GroupProfile = (props: Props) => {
  const [group, setGroup] = useState(props.group);
  const onMemberCreated = (member: T.Member): void =>
    setGroup(addMember(member, group));

  const onVolunteerSelected = (member: T.Member, selection: T.Selection) =>
    setGroup(updateMemberInGroup(addSelection(member, selection), group));

  return (
    <>
      <Volunteer group={group} onVolunteerSelected={onVolunteerSelected} />
      <br />
      <CreateMemberForm group={group} onMemberCreated={onMemberCreated} />
      <br />
      <Group group={group} />
      <Selections selections={toSelectionsProp(group)} />
    </>
  );
};

const toSelectionsProp = (group: T.Group): Selection[] =>
  flatten(
    group.members.map((member) =>
      member.selections.map((selection) => toSelectionProp(member, selection))
    )
  );

const flatten = (array: Selection[][]): Selection[] =>
  array.length > 0 ? array.reduce((prev, curr) => prev.concat(curr)) : [];

const toSelectionProp = (
  member: T.Member,
  selection: T.Selection
): Selection => ({
  id: selection.id,
  memberName: member.name,
  on: selection.on,
  for: selection.for,
});

export default GroupProfile;
