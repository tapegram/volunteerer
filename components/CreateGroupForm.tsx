import * as T from "../types";
import * as gql from "../data/fauna/generated/graphql";
import Button from "../components/Button";
import { CreateGroupDocument } from "../data/fauna/generated/graphql";
import { EitherAsync } from "purify-ts/EitherAsync";
import { initializeApollo } from "../apollo";
import { toGroup } from "../data/fauna/FaunaGroupsRepo";
import { useMutation } from "@apollo/client";
import { useState } from "react";

type Props = {
  onGroupCreated: (group: T.Group) => void;
};

const CreateGroupForm = (props: Props) => {
  const apolloClient = initializeApollo();
  const [groupName, setGroupName] = useState("");
  const [createGroup] = useMutation(CreateGroupDocument, {
    variables: { name: groupName },
    client: apolloClient,
  });

  const onCreateClicked = (): void => {
    EitherAsync(() => {
      try {
        return createGroup();
      } catch {
        throw Error;
      }
    })
      .map((result) =>
        props.onGroupCreated(toGroup(result.data!.createGroup as gql.Group))
      )
      .run();
  };

  return (
    <div className="w-3/5 mx-auto content-start md:content-around">
      <input
        className="px-3 py-3 placeholder-gray-400 text-gray-700 relative bg-white rounded text-sm shadow outline-none focus:outline-none focus:shadow-outline w-2/3"
        type="text"
        placeholder="new group name..."
        onChange={(e) => setGroupName(e.target.value)}
      />
      <span className="mx-2">
        <Button
          value="Create"
          onClick={onCreateClicked}
          disabled={groupName.trim().length < 1}
        />
      </span>
    </div>
  );
};

export default CreateGroupForm;
