import Link from "./Link";

const navStyles =
  "flex flex-col text-center sm:flex-row sm:text-left " +
  "sm:justify-between py-4 px-6 bg-white shadow sm:items-baseline w-full mb-4";

const NavBar = () => (
  <nav className={navStyles}>
    <span className="inset-y-0 left-0">
      <Link href="/" value="Volunteerer" />
    </span>
    <span className="inset-y-0 right-o mr-4">
      <Link href="/groups" value="Groups" />
    </span>
  </nav>
);

export default NavBar;
