import * as T from "../types";
import Link from "./Link";

type Props = {
  groups: T.Group[];
};

const Groups = (props: Props) => (
  <>
    <table className="bg-white pb-4 px-4 rounded-md w-full">
      <thead>
        <tr className="rounded-lg text-sm font-medium text-gray-700 text-left">
          <th className="px-4 py-2">Id</th>
          <th className="px-4 py-2">Name</th>
          <th className="px-4 py-2"></th>
        </tr>
      </thead>
      <tbody className="text-sm font-normal text-gray-700">
        {props.groups.map(toTableRow)}
      </tbody>
    </table>
  </>
);

const toTableRow = (group: T.Group) => (
  <tr
    key={group.id}
    className="hover:bg-gray-100 border-b border-gray-200 py-10"
  >
    <td className="px-4 py-4">{group.id}</td>
    <td className="px-4 py-4">{group.name}</td>
    <td className="px-4 py-4">
      <Link href={`/groups/${group.id}`} value="View" />
    </td>
  </tr>
);

export default Groups;
