import * as T from "../types";
import * as gql from "../data/fauna/generated/graphql";
import Button from "./Button";
import Modal from "./Modal";
import volunteer from "../behavior/Volunteer";
import { CreateSelectionDocument } from "../data/fauna/generated/graphql";
import { EitherAsync } from "purify-ts/EitherAsync";
import { Failure } from "../behavior/GroupsRepo";
import { Maybe } from "purify-ts/Maybe";
import { initializeApollo } from "../apollo";
import { toSelection } from "../data/fauna/FaunaGroupsRepo";
import { useMutation } from "@apollo/client";
import { useState } from "react";

type Props = {
  group: T.Group;
  onVolunteerSelected: (member: T.Member, selection: T.Selection) => void;
};

const Volunteer = (props: Props) => {
  const [volunteeredMember, setVolunteeredMember] = useState<Maybe<T.Member>>(
    Maybe.empty()
  );
  const [task, setTask] = useState("");
  const [failure, setFailure] = useState<Maybe<Failure>>(Maybe.empty());
  const [showModal, setShowModal] = useState(false);

  const apolloClient = initializeApollo();
  const [createSelection] = useMutation(CreateSelectionDocument, {
    client: apolloClient,
  });

  const onVolunteerClicked = () => {
    volunteer(props.group).either(
      (message) => setFailure(Maybe.of(message)),
      (member) => {
        setVolunteeredMember(Maybe.of(member));
        setShowModal(true);
      }
    );
  };

  const saveSelection = (task: string, on: Date, member: T.Member) =>
    EitherAsync(() => {
      try {
        return createSelection({
          variables: {
            for: task,
            on: on.toISOString(),
            memberId: member.id,
          },
        });
      } catch (error) {
        console.error(error);
        throw Error(error.message);
      }
    }).mapLeft((error) => (error as Error).message);

  const onConfirmAction = async (task: string, on: Date, member: T.Member) => {
    const result = await saveSelection(task, on, member).run();
    result.either(
      (message) => setFailure(Maybe.of(message)),
      (data) => {
        setFailure(Maybe.empty());
        props.onVolunteerSelected(
          member,
          toSelection(data.data?.createSelection as gql.Selection)
        );
      }
    );
    setShowModal(false);
  };

  return (
    <>
      <div className="w-4/5 mx-auto content-start md:content-around">
        <input
          className="px-3 py-3 placeholder-gray-400 text-gray-700 relative bg-white rounded text-sm shadow outline-none focus:outline-none focus:shadow-outline w-2/3"
          type="text"
          placeholder="task name..."
          onChange={(e) => setTask(e.target.value)}
        />
        <span className="mx-2">
          <Button
            value="Volunteer!"
            onClick={onVolunteerClicked}
            disabled={task.trim().length < 1}
          />
        </span>
      </div>

      {failure.caseOf({
        Just: (message) => <p>Oops: {message}!</p>,
        Nothing: () => <></>,
      })}

      {showModal
        ? volunteeredMember.caseOf({
            Just: (member) => (
              <Modal
                title="Congrats!"
                cancelLable="Nevermind"
                cancelAction={() => setShowModal(false)}
                confirmLable="Confirm"
                confirmAction={() =>
                  onConfirmAction(task, new Date(Date.now()), member)
                }
              >
                🎉 {member.name} 🎉
              </Modal>
            ),
            Nothing: () => <></>,
          })
        : null}
    </>
  );
};

export default Volunteer;
