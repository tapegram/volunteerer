type Props = {
  title: string;
  cancelLable: string;
  cancelAction: () => void;
  confirmLable: string;
  confirmAction: () => void;
  children: React.ReactNode;
};

const Modal = (props: Props) => (
  <>
    <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
      <div className="relative w-auto my-6 mx-auto max-w-3xl">
        <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
          <Header>{props.title}</Header>
          <Body>{props.children}</Body>
          <Footer
            cancelLable={props.cancelLable}
            cancelAction={props.cancelAction}
            confirmLable={props.confirmLable}
            confirmAction={props.confirmAction}
          />
        </div>
      </div>
    </div>
    <Background />
  </>
);

type HeaderProps = {
  children: React.ReactNode;
};
const Header = (props: HeaderProps) => (
  <div className="flex items-start justify-between p-5 border-b border-solid border-gray-300 rounded-t">
    <h3 className="text-3xl font-semibold">{props.children}</h3>
  </div>
);

type BodyProps = {
  children: React.ReactNode;
};
const Body = (props: BodyProps) => (
  <div className="relative p-6 flex-auto">
    <p className="my-4 text-gray-600 text-lg leading-relaxed">
      {props.children}
    </p>
  </div>
);

type FooterProps = {
  cancelLable: string;
  cancelAction: () => void;
  confirmLable: string;
  confirmAction: () => void;
};

const Footer = (props: FooterProps) => (
  <div className="flex items-center justify-end p-6 border-t border-solid border-gray-300 rounded-b">
    <button
      className="text-red-500 background-transparent font-bold uppercase px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1"
      type="button"
      style={{ transition: "all .15s ease" }}
      onClick={() => props.cancelAction()}
    >
      {props.cancelLable}
    </button>
    <button
      className="bg-green-500 text-white active:bg-green-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1"
      type="button"
      style={{ transition: "all .15s ease" }}
      onClick={() => props.confirmAction()}
    >
      {props.confirmLable}
    </button>
  </div>
);

const Background = () => (
  <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
);

export default Modal;
