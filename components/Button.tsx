type Props = {
  value: string;
  onClick: () => void;
  disabled: boolean;
};

const buttonStyle =
  "bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded";
const disabledButtonStyle =
  "bg-gray-300 text-white font-bold py-2 px-4 rounded";

const Button = (props: Props) => (
  <>
    <input
      type="button"
      className={props.disabled ? disabledButtonStyle : buttonStyle}
      value={props.value}
      onClick={props.onClick}
      disabled={props.disabled}
    />
  </>
);

export default Button;
