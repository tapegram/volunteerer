import Link from "next/link";

type Props = {
  href: string;
  value: string;
};

const linkStyle =
  "text-pink-500 background-transparent font-bold uppercase px-3 py-1 text-xs outline-none focus:outline-none mr-1 mb-1";

const Button = (props: Props) => (
  <div className={linkStyle} style={{ transition: "all .15s ease" }}>
    <Link href={props.href}>
      <a>{props.value}</a>
    </Link>
  </div>
);

export default Button;
