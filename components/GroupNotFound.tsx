type Props = {
  id: string;
};

const GroupNotFound = (props: Props) => <p>Group {props.id} was not found</p>;

export default GroupNotFound;
