export type Selection = {
  id: string;
  memberName: string;
  on: Date;
  for: string;
};

type Props = {
  selections: Selection[];
};

const Selections = (props: Props) => (
  <>
    <h1 className="mx-auto">Selections</h1>
    <table className="bg-white pb-4 px-4 rounded-md w-full">
      <thead>
        <tr className="rounded-lg text-sm font-medium text-gray-700 text-left">
          <th className="px-4 py-2">Volunteer</th>
          <th className="px-4 py-2">on</th>
          <th className="px-4 py-2">for</th>
        </tr>
      </thead>
      <tbody className="text-sm font-normal text-gray-700">
        {props.selections
          .sort(
            (a: Selection, b: Selection) =>
              new Date(b.on).getTime() - new Date(a.on).getTime()
          )
          .map(toTableRow)}
      </tbody>
    </table>
  </>
);

const toTableRow = (selection: Selection) => (
  <tr
    key={selection.id}
    className="hover:bg-gray-100 border-b border-gray-200 py-10"
  >
    <td className="px-4 py-4">{selection.memberName}</td>
    <td className="px-4 py-4">{selection.on}</td>
    <td className="px-4 py-4">{selection.for}</td>
  </tr>
);

export default Selections;
