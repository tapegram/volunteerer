import * as T from "../types";
import * as gql from "../data/fauna/generated/graphql";
import Button from "../components/Button";
import { initializeApollo } from "../apollo";
import { toMember } from "../data/fauna/FaunaGroupsRepo";
import { useMutation } from "@apollo/client";
import { useState } from "react";
import { EitherAsync } from "purify-ts/EitherAsync";

type Props = {
  group: T.Group;
  onMemberCreated: (member: T.Member) => void;
};

const CreateMemberForm = (props: Props) => {
  const apolloClient = initializeApollo();
  const [memberName, setMemberName] = useState("");
  const [createMember] = useMutation(gql.CreateMemberDocument, {
    variables: { name: memberName, groupId: props.group.id },
    client: apolloClient,
  });

  const onCreateClicked = (): void => {
    EitherAsync(() => {
      try {
        return createMember();
      } catch {
        throw Error;
      }
    })
      .map((result) =>
        props.onMemberCreated(toMember(result.data!.createMember as gql.Member))
      )
      .run();
  };

  return (
    <div className="w-4/5 mx-auto content-start md:content-around">
      <input
        className="px-3 py-3 placeholder-gray-400 text-gray-700 relative bg-white rounded text-sm shadow outline-none focus:outline-none focus:shadow-outline w-2/3"
        type="text"
        placeholder="new member name..."
        onChange={(e) => setMemberName(e.target.value)}
      />
      <span className="mx-2">
        <Button
          value="Create"
          onClick={onCreateClicked}
          disabled={memberName.trim().length < 1}
        />
      </span>
    </div>
  );
};

export default CreateMemberForm;
