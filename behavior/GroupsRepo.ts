import { EitherAsync } from "purify-ts/EitherAsync";
import { Group } from "../types"

export type Failure = string

export type GroupsRepo = {
    fetchAll: () => EitherAsync<Failure, Group[]>;
    findById: (id: string) => EitherAsync<Failure, Group>
};
