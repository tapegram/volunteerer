import volunteer from "./Volunteer";
import * as T from "../types";
import { Left, Right } from "purify-ts/Either";

const toGroup = (members: T.Member[]): T.Group => ({
  id: "1",
  name: "Matcha",
  members: members,
});

const toMember = (id: string, name: string): T.Member => ({
  id: id,
  name: name,
  selections: [],
});

const toEmptyGroup = (): T.Group => toGroup([]);
const toGroupWith1Member = (): T.Group => toGroup([toMember("1", "Billy")]);
const toGroupWith2Members = (): T.Group =>
  toGroup([toMember("1", "Billy"), toMember("2", "Bob")]);

describe("Volunteer", () => {
  it("should fail if group is empty", () => {
    expect(volunteer(toEmptyGroup())).toEqual(
      Left("Can not select a volunteer from an empty group")
    );
  });

  it("should pick the only member of a single person group", () => {
    expect(volunteer(toGroupWith1Member())).toEqual(
      Right(toGroupWith1Member().members[0])
    );
  });

  // Good opportunity for property testing here. This test is insufficient.
  it("should randomly pick a member of a multiperson group", () => {
    expect(toGroupWith2Members().members.map(Right)).toContainEqual(
      volunteer(toGroupWith2Members())
    );
  });
});
