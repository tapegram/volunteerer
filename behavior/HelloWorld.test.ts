import greet from "./HelloWorld"

describe("Greet", () => {
    it("should say hello based on the name", () => {
        expect(greet("World")).toEqual("Hello, World!");
    });
})
