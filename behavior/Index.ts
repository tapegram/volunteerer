import { Maybe } from "purify-ts/Maybe";
import * as T from "../types";

export const addGroup = (group: T.Group, groups: T.Group[]) =>
  groups.concat(group);

export const addSelection = (
  member: T.Member,
  selection: T.Selection
): T.Member => ({
  ...member,
  selections: member.selections.concat(selection),
});

export const removeMember = (memberId: string, group: T.Group): T.Group => ({
  ...group,
  members: group.members.filter((it) => it.id != memberId),
});

export const addMember = (member: T.Member, group: T.Group): T.Group => ({
  ...group,
  members: group.members.concat(member),
});

export const updateMemberInGroup = (
  member: T.Member,
  group: T.Group
): T.Group => addMember(member, removeMember(member.id, group));

export const findGroupById = (id: string, groups: T.Group[]) =>
  Maybe.fromNullable(groups.find((it) => it.id == id));

export const getSelections = (group: T.Group): T.Selection[] =>
  flatten(group.members.map((member) => member.selections));

export const flatten = (array: T.Selection[][]): T.Selection[] =>
  array.length > 0 ? array.reduce((prev, curr) => prev.concat(curr)) : [];
