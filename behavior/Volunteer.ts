import { Either, Left, Right } from "purify-ts/Either";
import * as T from "../types";
import { Failure } from "./GroupsRepo";

const volunteer = (group: T.Group): Either<Failure, T.Member> => {
  if (group.members.length < 1) {
    return Left("Can not select a volunteer from an empty group");
  }
  return Right(group.members[Math.floor(Math.random() * group.members.length)]);
};

export default volunteer;
