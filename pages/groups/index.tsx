import * as T from "../../types";
import CreateGroupForm from "../../components/CreateGroupForm";
import Groups from "../../components/Groups";
import Layout from "../../components/Layout";
import toFaunaGroupsRepo from "../../data/fauna/FaunaGroupsRepo";
import { Either } from "purify-ts/Either";
import { Failure } from "../../behavior/GroupsRepo";
import { initializeApollo } from "../../apollo";
import { useState } from "react";
import { addGroup } from "../../behavior/Index";

type Props = {
  groups: T.Group[];
};

const Index = (props: Props) => {
  const [groups, setGroups] = useState(props.groups);
  const onGroupCreated = (group: T.Group): void =>
    setGroups(addGroup(group, groups));

  return (
    <Layout>
      <CreateGroupForm onGroupCreated={onGroupCreated} />
      <br />
      <Groups groups={groups} />
    </Layout>
  );
};

export async function getServerSideProps() {
  const apolloClient = initializeApollo();

  const result: Either<Failure, T.Group[]> = await toFaunaGroupsRepo(
    apolloClient
  )
    .fetchAll()
    .run();
  const props: Props = {
    groups: result.either(
      () => [],
      (groups) => groups
    ),
  };
  return { props };
}

export default Index;
