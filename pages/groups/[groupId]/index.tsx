import * as T from "../../../types";
import GroupProfile from "../../../components/GroupProfile";
import Layout from "../../../components/Layout";
import toFaunaGroupsRepo from "../../../data/fauna/FaunaGroupsRepo";
import { Failure } from "../../../behavior/GroupsRepo";
import { findGroupById } from "../../../behavior/Index";
import { Maybe } from "purify-ts/Maybe";
import { initializeApollo } from "../../../apollo";
import { useState } from "react";
import GroupNotFound from "../../../components/GroupNotFound";

type Props = {
  id: string;
  groups: T.Group[];
  failure: Failure | null;
};

const Index = (props: Props) => {
  const [groups] = useState(props.groups);
  const [group] = useState(findGroupById(props.id, groups));
  const failure = Maybe.fromNullable(props.failure);

  return (
    <Layout>
      {failure.caseOf({
        Just: (message) => <p>Oops! {message}</p>,
        Nothing: () => <></>,
      })}
      {group.caseOf({
        Just: (group) => <GroupProfile group={group} />,
        Nothing: () => <GroupNotFound id={props.id} />,
      })}
    </Layout>
  );
};

export async function getServerSideProps(context: any) {
  const apolloClient = initializeApollo();
  const result = await toFaunaGroupsRepo(apolloClient).fetchAll().run();

  return {
    props: result.either(
      (failure: Failure) => toFailure(failure, context.params.groupId),
      (groups: T.Group[]) => toSuccess(groups, context.params.groupId)
    ),
  };
}

const toFailure = (failure: Failure, id: string): Props => ({
  id: id,
  groups: [],
  failure: failure,
});

const toSuccess = (groups: T.Group[], id: string): Props => ({
  id: id,
  groups: groups,
  failure: null,
});

export default Index;
