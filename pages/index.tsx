import Layout from "../components/Layout"

const IndexPage = () => (
  <Layout>
    <h1>Hello Next.js 👋</h1>
    <p>Deploying with Vercel!</p>
  </ Layout>
)

export default IndexPage
